package bs.mod1;

import org.junit.Test;

import bs.mod1.Mod1Impl;

import junit.framework.Assert;

public class Mod1ImplTest {
    @Test
    public void testHello() {
        Assert.assertEquals("Hello there aaargh", (new Mod1Impl()).hello("aaargh"));
    }
}