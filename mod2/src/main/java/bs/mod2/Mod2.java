package bs.mod2;

import bs.mod1.Mod1;

public class Mod2 {
    private final Mod1 mod1;

    public Mod2(Mod1 mod1) {
        super();
        this.mod1 = mod1;
    }

    public String go() {
        // bad code for pmd
        String s = "";
        try {
            s = "Mod 1 says: '" + mod1.hello("mod2") + "'";
        } catch (Throwable e) {
        }
        // TODO dummy taglist content in mod2
        return s;
    }
}
