package bs.mod2;

import org.junit.Test;

import bs.mod1.Mod1;
import bs.mod2.Mod2;

import junit.framework.Assert;

public class Mod2Mod1IT {
    @Test
    public void testGo() {
        Mod1 mod1Mock = new Mod1() {

            @Override
            public String hello(String name) {
                return "from mod1Mock";
            }
            
        };
        Assert.assertEquals("Mod 1 says: 'from mod1Mock'", (new Mod2(mod1Mock)).go());
    }
}