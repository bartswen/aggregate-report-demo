package bs.mod2;

import org.junit.Test;

import bs.mod1.Mod1Impl;
import bs.mod2.Mod2;

import junit.framework.Assert;

public class Mod2Test {
    @Test
    public void testGo() {
        Assert.assertEquals("Mod 1 says: 'Hello there mod2'", (new Mod2(new Mod1Impl())).go());
    }
}